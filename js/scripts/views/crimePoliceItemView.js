define([
	'backbone',
	'marionette',
	'handlebars',
	'text!./../../../templates/tables/tableRow-template.html',
	'async!http://maps.google.com/maps/api/js',
	'odometer',
	'randomColor',
	'bootstrap',
], function (Backbone, Marionette, Handlebars, tableRowTemplate, gmaps, Odometer, RandomColor, Bootstrap){

	return Marionette.ItemView.extend({
		el : ".table-container",
		template : Handlebars.compile(tableRowTemplate),
		maps : [],
		categoryColors : [],
		events : {
			'click .button-maps-visibility' : 'showHideMapPerCategory',
			'click .button-launch-modal' : 'openModalWithData',
		},
		initialize : function(){
			this.generateMapsPerCategory(this.collection.toJSON()[0]);
		},
		generateMapsPerCategory : function(data){
			var self = this;
			var categoriesLength = data.categories.length -1;
			_.each(data.categories, function(category, position){
				self.parseMapData(data.records[category], position);
				self.odometerInitialize(data.records[category], position);
			});
		},
		getMapOptions : function(center, zoom, mapType, interactiveMaps){
			var mapInitOptions = {
		    center		: center,
		    zoom 			: zoom,
		    MapTypeId : google.maps.MapTypeId[mapType],
		  };

		  if(interactiveMaps){
		  	var disableUi = {
			  	disableDefaultUI : true,
			    disableDoubleClickZoom : true,
			    draggable : false,
			    scrollwheel : false
		  	};
		  	_.extend(mapInitOptions, disableUi);
		  }

			return mapInitOptions;
		},
		getGoogleLtLgn : function(latitude, longitude){
			return new google.maps.LatLng(latitude, longitude);
		},
		createMapWithOption : function(mapContainer, mapInitOptions){
			return  new google.maps.Map(document.getElementById(mapContainer), mapInitOptions);
		},
		parseMapData : function(record, recordPosition){
			var crimeMarkerPosition = this.getGoogleLtLgn(record.data[0].latitude, record.data[0].longitude);
			var self = this;
			var mapInitOptions = self.getMapOptions(crimeMarkerPosition, 6, 'ROADMAP', true);

		  setTimeout(function(){
		  	var mapInstance = self.createMapWithOption("mapCategory_" + recordPosition, mapInitOptions);
			  self.maps.push(mapInstance);

		  	google.maps.event.addListener(mapInstance, 'bounds_changed', function() {
		  		_.each(record.data, function(record){
		  			var location = new google.maps.LatLng(record.latitude, record.longitude);
		      	self.addMarkerToMapInstance(mapInstance, location);
		  		});
			  	$("#tableRowMap_"+recordPosition).hide();
		    });
		  },100);
		},
		addMarkerToMapInstance : function(mapInstance, markerLocation){
			var marker = new google.maps.Marker({
        position: markerLocation,
        map: mapInstance
	    });
		},
		showHideMapPerCategory : function(event){
			var buttonElement = $(event.target);
			var mapRowElement = $("#tableRowMap_" + buttonElement.attr('data-map'));

			if( $(mapRowElement).is(":visible") ){
				buttonElement.removeClass('btn-primary').addClass('btn-default').text('Show Reports');
				mapRowElement.fadeOut('slow');
			} else {
				buttonElement.removeClass('btn-default').addClass('btn-primary').text('Hide Reports');
				mapRowElement.fadeIn('slow');
			}
		},
		addNewOdometer : function(elementReference, value){
			var averageOdometer = new Odometer({
				el : document.querySelector(elementReference),
				value : 0,
			});
			averageOdometer.update(value);
		},
		odometerInitialize : function(record, recordPosition){
			var self = this;
			setTimeout(function(){
				$("#odometer_" + recordPosition).css({'background' : self.getRandomColor() });
				self.addNewOdometer("#odometer_" + recordPosition, record.items);
			},1500);
		},
		getRandomColor : function(){
			var color = RandomColor({luminosity: 'dark'});
			if( _.contains(this.categoryColors, color) ){
				var color = RandomColor({luminosity: 'dark'});
			}
			this.categoryColors.push(color);
			return color;
		},
		openModalWithData : function(event){
			event.stopPropagation();
			var self = this;
			var mapIdentifier = $(event.target).attr('data-map');
			var data = this.collection.toJSON()[0];
			var category = data.categories[mapIdentifier];
			var records = data.records[category];

			$('#mapDetail').find('.modal-title').text(records.name);
			$('#mapDetail').find('.modal-title').append('<span class="badge custom-badge">' + records.data.length + '</span>');
			$('#mapDetail').find('.modal-title .badge').css({background : this.categoryColors[mapIdentifier]});

			$('#mapDetail').modal('show').on('shown.bs.modal', function(){
				var mapProp = self.getMapOptions(self.getGoogleLtLgn(records.data[0].latitude, records.data[0].longitude), 5, 'ROADMAP', false);
			  var modalMap = self.createMapWithOption("map-canvas-interactive", mapProp);

			  google.maps.event.addListener(modalMap, 'bounds_changed', function() {
		  		_.each(records.data, function(record){
		  			var location = new google.maps.LatLng(record.latitude, record.longitude);
		      	self.addMarkerToMapInstance(modalMap, location);
		  		});
		    });
			});
		}
	});
});