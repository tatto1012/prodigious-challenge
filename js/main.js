require.config({
  baseUrl: '.',
  paths : {
    jquery      : '../bower_components/jquery/dist/jquery.min',
    underscore  : '../bower_components/underscore/underscore-min',
    backbone    : '../bower_components/backbone/backbone-min',
    marionette  : '../bower_components/backbone.marionette/lib/backbone.marionette.min',
    handlebars  : '../bower_components/handlebars/handlebars.min',
    bootstrap   : '../bower_components/bootstrap-sass/assets/javascripts/bootstrap.min',
    text        : '../bower_components/text/text',
    async       : '../bower_components/requirejs-plugins/src/async',
    odometer    : '../bower_components/odometer/odometer.min',
    randomColor : '../bower_components/randomcolor/randomColor',
  },
  shim : {
    jquery: {
      exports: '$'
    },
    underscore: {
      exports: '_'
    },
    backbone: {
      deps: ['underscore', 'jquery'],
      exports: 'Backbone'
    },
    marionette : {
      deps: ['backbone'],
      exports: 'Marionette'
    },
    handlebars : {
      exports : 'Handlebars'
    },
    text: {
      exports: 'text'
    },
    bootstrap : {
      deps : ['jquery'],
      exports : 'Bootstrap'
    }
  }
});

require([
  'backbone',
  'js/app',
  'marionette',
  'js/scripts/collections/crimePoliceCollection',
  'js/scripts/views/crimePoliceItemView',
  'js/scripts/views/generalView',
], function (Backbone, app, Marionette, CrimePoliceCollection, CrimePoliceView, GeneralView) {

  var CrimePolice = new CrimePoliceCollection();
  CrimePolice.fetch({
    success : function(crimesCollection, parsedData, options){
      app.start(GeneralView);

      var collectionView = new CrimePoliceView({ collection : crimesCollection});
      collectionView.render();
    }
  });
});