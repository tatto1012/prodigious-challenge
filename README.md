## Prodigious Ramp-Up challenge ##
Challenge builded by Jonathan Rodriguez <alexander.rodriguez@prodigious.com>.

This is a tecnical challenge for prodigious ramp-up process. that use the next tecnologies and libraries.
* Backbone JS.
* Marionette JS.
* Require JS.
* Jquery.
* Underscore.
* HandleBars.
* SASS.
* Grunt.
* Node JS.
* Bootstrap.
* RandomColor.
* Google Maps.
* Odometer.

# Get Starting #

## Requirements ##
* node (v5.2.0) or later.
* npm (3.3.12) or later.
* bower (1.7.1) or later.
* grunt-cli (v0.1.13) or later.
* sass (3.4.20) or later

## Prepare the environment ##
get and prepare the local dependencies from the .json files ```npm install``` and  ```bower install```.

## Run Grunt Task ##
Prapare and put the project ro tun on the browser for local develoment.

* Run the default task ```grunt```.
* Run the default develoment task ```grunt dev```.
* Only build the styles.css file ```grunt sass:styles```.
* Only build the styles.css file ```grunt sass:vendor```.
* Put the production environemnt ```grunt live``` or ```grunt``` and visit [http://localhost:3000](http://localhost:3000).