define([
	'backbone',
	'marionette',
	'handlebars',
	'bootstrap',
	'text!./../../../templates/modals/spinner-template.html',
	'text!./../../../templates/modals/map-modal-template.html',
	'text!./../../../templates/content-pages/widgets-tempalte.html',
	'text!./../../../templates/content-pages/main-table-template.html',
], function (Backbone, Marionette, Handlebars, Bootstrap, spinnerTemplate, mapModalTemplate, widgetsTemplate, mainTableTemplate){

	return Marionette.ItemView.extend({
		el : 'body',
		initialize : function(){
			this.appendModalsContent();
			this.appendContent();
			this.launchSpinner();
		},
		appendModalsContent : function(){
			$(this.$el).append(spinnerTemplate);
			$(this.$el).append(mapModalTemplate);
		},
		appendContent : function(){
			$(this.$el).find('#content').append(widgetsTemplate);
			$(this.$el).find('#content').append(mainTableTemplate);
		},
		launchSpinner : function(){
			$('#spinner').modal('show');
			setTimeout(function(){
				$('#spinner').modal('hide');
			},1000);
		}
	});
});