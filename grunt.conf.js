module.exports = function(grunt){
  var self = this;

  self.gruntTask = {};

  self.addSassTask = function(fileStyle, destination, source){
    var files = {};
    files[destination] = source;   // 'destination': 'source'

    return {
      options: {                   // Target options
        style: fileStyle,           // types ['expanded', 'compressed', 'nested', 'compact']
      },
      files: files                 //source and destinations compile
    };
  };

  self.addConcatTask = function(fileNameBanner, destination, sourceFiles){
    var bannerText = self.generateNameComment(fileNameBanner);
    return {
      options: {                                //Generate a custome separator
        banner : bannerText,                    //This string will be prepended to the beginning of the concatenated output.
        process : function(src, filepath){      //Customize the each output prepended
          return '\n/* ============= ' + filepath + ' ========== -_- =======*/\n' + src.replace(/(^|\n)[ \t]*('use strict'|"use strict");?\s*/g, '$1');
        }
      },
      src: sourceFiles,                         //Dictonary of files to concatenate
      dest: destination,                        //Outpus destination file
    };
  };

  self.addWatchTask = function(tasks, options, watchFilesList){
    var taskOptions = {};

    if(tasks){
      taskOptions['tasks'] = tasks;
    }

    if(options){
      taskOptions['options'] = options;
    }

    if(watchFilesList){
      taskOptions['files'] = watchFilesList;
    }

    return taskOptions;
  };

  self.addServerTask = function(hostname, port, openDefaultBrowser){
    return {
      options: {
        hostname : hostname,
        port: port,
        keepalive : false,
        livereload : false,
        open : openDefaultBrowser
      }
    };
  };

  self.generateNameComment = function(commentReference){
    var bannerText = '';
    var limitText = 50, padding = 10;
    bannerText = '/********************************************************************/\n';
    bannerText+= '/********************************************************************/\n';
    bannerText+= '/******   ';
    bannerText+= self.refillComment(limitText, padding, commentReference);
    bannerText+= '   ******/\n';
    bannerText+= '/******              <%= pkg.name %> / <%= pkg.version %>              ******/\n';
    bannerText+= '/********************************************************************/\n';
    bannerText+= '/********************************************************************/\n\n';

    return bannerText;
  };

  self.refillComment = function(limit, padding, text){
    var textLength = text.length;
    if(textLength >= limit){
      return text.substring(0, limit - 1);
    } else {
      var paddingRemaining = (limit - textLength) / 2;
      var leftPadding, rigthPadding;

      if(paddingRemaining % 1 === 0){
        leftPadding = rigthPadding = paddingRemaining;
      } else {
        leftPadding = Math.floor(paddingRemaining);
        rigthPadding = Math.round(paddingRemaining);
      }

      text =  text + new Array(leftPadding + 1 ).join(' '); // rigth padding
      text =  new Array(rigthPadding + 1).join(' ') + text; // left padding

      return text;
    }
  };

  //Define the package
	self.gruntTask['pkg'] = grunt.file.readJSON('package.json');

  //define the SASS task
	self.gruntTask['sass'] = {
    styles: self.addSassTask('compact', 'css/styles.css', 'css/scss/app.scss'),
    vendor: self.addSassTask('compact', 'css/vendor.css', 'css/scss/vendor.scss')
  };

  //define the concatenate files
	self.gruntTask['concat'] = {
    vendor : self.addConcatTask('Application Vendor Prodigious Technical Challenge', 'js/vendor.js', [
      'bower_components/jquery/dist/jquery.min.js',
      'bower_components/underscore/underscore-min.js',
      'bower_components/backbone/backbone-min.js',
      'bower_components/backbone.marionette/lib/backbone.marionette.min.js',
      'bower_components/handlebars/handlebars.min.js',
      'bower_components/bootstrap-sass/assets/javascripts/bootstrap.min.js',
      'bower_components/requirejs/require.js',
    ]),
    app : self.addConcatTask('Application  App Prodigious Technical Challenge', 'js/app.js', [
      'js/scripts/main.js',
      'js/scripts/models/*.js',
      'js/scripts/collections/*.js',
      'js/scripts/views/*.js',
      'js/scripts/routers/*.js',
    ]),
    vendorCss : self.addConcatTask('Global Css Features Styless', 'css/features.css', [
      'bower_components/odometer/themes/odometer-theme-default.css'
    ])
  };

  //define the files changes listeners
  self.gruntTask['watch'] = {
    options : {
      livereload: true
    },
    html : self.addWatchTask(false, { livereload: true }, [
      'index.html',
      'templates/*.html',
      'templates/**/*.html'
    ]),
    appJs: self.addWatchTask(false, { livereload: true }, [
      'js/*.js',
      'js/**/*.js',
      'js/**/**/*.js'
    ]),
    sassStyles: self.addWatchTask('sass:styles', { livereload: true }, [
      'css/scss/app.scss',
      'css/scss/prodigious-challenge/*.scss'
    ]),
    sassVendor: self.addWatchTask('sass:vendor', { livereload: true }, [
      'css/scss/vendor.scss',
      'css/scss/bootstrap/*.scss'
    ])
  };

  //define and put server live
  self.gruntTask['connect'] = {
    serverDev: self.addServerTask('localhost', 3000, true),
    serverLive: self.addServerTask('localhost', 3000, false)
  };

  //return the task configuration
	return {
		gruntConfig : self.gruntTask
	};
};