define(function(require){
	var _ = require('underscore');
  var $ = require('jquery');
  var Backbone = require('backbone');
  var Marionette = require('marionette');

	var app = new Marionette.Application();

	app.on('start', function(GeneralView){
    console.log("App initialized!!");
	  var generalView = new GeneralView({});
  });

	return app;
});