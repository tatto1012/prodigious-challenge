module.exports = function(grunt) {
//load grunt congfig
  var _config = require('./grunt.conf.js')(grunt);
//Load the dependencies
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-connect');
// Project configuration.
  grunt.initConfig(_config.gruntConfig);

// Default task(s). compile sass and put live environment
  grunt.registerTask('default', ['sass', 'concat:vendorCss', 'live']);
//Development Environment
  grunt.registerTask('dev', ['concat:vendorCss', 'connect:serverDev', 'watch']);
//Production Environment
  grunt.registerTask('live', ['connect:serverLive:keepalive']);

};