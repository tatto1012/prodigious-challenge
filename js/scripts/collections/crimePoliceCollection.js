define([
	'backbone',
	'../models/crimePoliceModel'
], function (Backbone, CrimePoliceModel){

	return Backbone.Collection.extend({
		model : CrimePoliceModel,
		url : 'https://data.police.uk/api/crimes-street/all-crime?poly=52.268,0.543:52.794,0.238:52.130,0.478&date=2013-01',
		parse : function(responseData){
			var parseData = {
				categories : [],
				records : {}
			};
			_.each(responseData, function(itemData){
				if( !_.contains(parseData.categories, itemData.category) ){
					parseData.categories.push(itemData.category);

					parseData.records[itemData.category] = {
						data : [ {latitude : itemData.location.latitude, longitude : itemData.location.longitude} ],
						items : 1,
						name : itemData.category
					};
				} else {
					parseData.records[itemData.category]['data'].push({ latitude : itemData.location.latitude, longitude : itemData.location.longitude});
					parseData.records[itemData.category]['items']++;
				}
			});
			return parseData;
		}
	});

});